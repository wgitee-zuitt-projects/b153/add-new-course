import { useState, useEffect } from 'react';
import { Table, Button, Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function AdminView(props){

	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState("")


	const addCourse = (e) => {
		//prevent page redirect via form submission
		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/courses`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			//console.log(data)

			if(data){
				
				Swal.fire({
					title: "Add New Course Successful",
					icon: "success",
					text: `${name} course added successfully!`
				})
				fetchData()
				closeAdd()

			}else{
				
				Swal.fire({
					title: "Course Registration Failed!",
					icon: "error",
					text: "Please try again."
				})
				fetchData()
			}
		})
	}


	// console.log(props)

	const { coursesProp, fetchData } = props;

	const [coursesArr, setCoursesArr] = useState([])
	const [showAdd, setShowAdd] = useState(false)
	const [showEdit, setShowEdit] = useState(false)

	const token = localStorage.getItem("token")

	//Functions to handle opening and closing modals
	const openAdd = () => setShowAdd(true)
	const closeAdd = () => setShowAdd(false)
	const openEdit = () => setShowEdit(true)
	const closeEdit = () => setShowEdit(false)

	const archiveToggle = (courseId, isActive) => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}/archive`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				isActive: !isActive
			})
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data)

			
			if(data){
				let bool

				isActive ? bool = "disabled" : bool = "enabled"

				fetchData()
				Swal.fire({
					title: "Success",
					icon: "success",
					text: `Course successfully ${bool}`
				})
			}else{
				fetchData()

				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again"
				})
			}
		})
	}

	useEffect(() => {
		const courses = coursesProp.map(course => {
			return(
				<tr key={course._id}>
					<td>{course.name}</td>
					<td>{course.description}</td>
					<td>{course.price}</td>
					<td>
							{course.isActive
								? <span>Available</span>
								: <span>Unavailable</span>
							}
					</td>
					<td>
						<Button variant="primary" size="sm" onClick={openEdit}>Update</Button>
						{course.isActive
							? <Button variant="danger" size="sm" onClick={() => archiveToggle(course._id, course.isActive)}>Disable</Button>
							: <Button variant="success" size="sm" onClick={() => archiveToggle(course._id, course.isActive)}>Enable</Button>
						}
					</td>
				</tr>
			)
		})

		setCoursesArr(courses)

	}, [coursesProp])

	
	return(
		<>
			<div className="text-center my-4">
				<h2>Admin Dashboard</h2>
				<Button variant="primary" onClick={openAdd}>Add New Course</Button>			
			</div>
		{/*View course table*/}
			<Table striped bordered hover responsive>
				<thead className="bg-dark text-white">
					<tr>
						<th>Name</th>
						<th>Description</th>
						<th>Price</th>
						<th>Availability</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{coursesArr}
				</tbody>
			</Table>

		{/*Add course Modal*/}
		{/*
			Activity to be checked on Monday:
				Complete the add course form
				- No data validation necessary
				- Make sure to call the fetchData() function when you receive a true response from the server
				- When done, only push AdminView.js to Gitlab
		*/}

			<Modal show={showAdd} onHide={closeAdd}>
				<Form onSubmit={e => addCourse(e)} className="my-3">
					<Modal.Header closeButton>
						<Modal.Title>Add Course</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form.Group controlId="courseName">
							<Form.Label>Name</Form.Label>
							<Form.Control 
								type="text"
								placeholder="Enter course name"
								value={name}
								onChange={e => setName(e.target.value)}
								required
							/>
						</Form.Group>

						<Form.Group controlId="courseDescription">
							<Form.Label>Description</Form.Label>
							<Form.Control 
								type="text"
								placeholder="Enter course description"
								value={description}
								onChange={e => setDescription(e.target.value)}
								required
							/>
						</Form.Group>

						<Form.Group controlId="coursePrice">
							<Form.Label>Price</Form.Label>
							<Form.Control 
								type="number"
								placeholder="Enter course price"
								value={price}
								onChange={e => setPrice(e.target.value)}
								required
							/>
						</Form.Group>
					</Modal.Body>

					<Modal.Footer>
						<Button variant="secondary" onClick={closeAdd}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>

		{/*Edit Course Modal*/}
			<Modal show={showEdit} onHide={closeEdit}>
				<Form>
					<Modal.Header closeButton>
						<Modal.Title>Update Course</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form.Group controlId="courseName">
							<Form.Label>Name</Form.Label>
							<Form.Control type="text"/>
						</Form.Group>

						<Form.Group controlId="courseDescription">
							<Form.Label>Description</Form.Label>
							<Form.Control type="text"/>
						</Form.Group>

						<Form.Group controlId="coursePrice">
							<Form.Label>Price</Form.Label>
							<Form.Control type="number"/>
						</Form.Group>
					</Modal.Body>

					<Modal.Footer>
						<Button variant="secondary" onClick={closeEdit}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>
		</>
	)
}
